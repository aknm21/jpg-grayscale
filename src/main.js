import Vue from 'vue'
// import App from './App.vue'
window.Vue = require('vue');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import imageList from './components/ImageList.vue'
Vue.component(
  'image-list',
  imageList
);

import imagePost from './components/ImagePost.vue'
Vue.component(
  'image-post',
  imagePost
);

const app = new Vue({
  el: '#app',
})
