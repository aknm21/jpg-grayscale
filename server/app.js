const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const multer = require("multer");
const fs = require("fs");

const db = require("./database.js");

const jpeg = require("jpeg-js");

const storage = multer.diskStorage({
  // ファイルの保存先指定
  destination: function(req, file, cb) {
    cb(null, "server/public/images");
  },
  // ファイル名指定
  filename: function(req, file, cb) {
    //　Math.random().toString(36).slice(-9)で乱数を生成
    const imageName = `${Math.random()
      .toString(36)
      .slice(-9)}_${Date.now()}.jpg`;
    cb(null, imageName);
  }
});
const upload = multer({
  storage: storage
}).single("file");

const app = express();

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/public"));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

/* 一覧画面 */
app.get("/", (req, res) => {
  res.render("index", { title: "一覧ページ" });
});

/* 投稿画面 */
app.get("/post", (req, res) => {
  res.render("post", { title: "投稿ページ" });
});

/* API */
app.get("/images", (req, res) => {
  const sql = "select * from images";
  const params = [];
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    console.log(rows);
    res.json({
      message: "success",
      images: rows
    });
  });
});

app.post("/image", upload, (req, res) => {
  if (!req.body.title || !req.file) {
    res.status(400).json({
      message: "title か file が入ってません。"
    });
  }
  const file = req.file;
  const meta = req.body;

  // JPEG 処理
  const jpegData = fs.readFileSync(file.path);
  const rawImageData = jpeg.decode(jpegData, true);
  const frameData = Buffer.alloc(rawImageData.data.length);
  let i = 0;
  while (i < rawImageData.data.length) {
    const R = rawImageData.data[i];
    const G = rawImageData.data[i + 1];
    const B = rawImageData.data[i + 2];

    const X = 2.2;

    const RGamma = (R / 255) ** X * 255;
    const GGamma = (G / 255) ** X * 255;
    const BGamma = (B / 255) ** X * 255;
    const rgbValue =
      ((RGamma*0.2126 + GGamma*0.7152 + BGamma*0.0722) / 255) ** (1 / X) * 255

    frameData[i] = rgbValue;
    frameData[i + 1] = rgbValue;
    frameData[i + 2] = rgbValue;
    frameData[i + 3] = 0xff; // alpha
    i += 4;
  }
  const jpegImageData = jpeg.encode(
    {
      data: frameData,
      width: rawImageData.width,
      height: rawImageData.height
    },
    100
  );
  fs.writeFileSync(file.path, jpegImageData.data);

  const data = {
    title: meta.title,
    filename: file.filename,
    path: file.path
  };
  const sql = "INSERT INTO images (title, filename, path) VALUES (?,?,?)";
  const params = [data.title, data.filename, data.path];
  db.run(sql, params, (err, result) => {
    if (err) {
      console.log(err);
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: "success",
      data: data,
      id: this.lastID
    });
  });
});
// app.put("/image/:imageId", (req, res) => {
//   return res.send(`PUT HTTP method on image/${req.params.imageId} resource`);
// });
app.delete("/image/:imageId", (req, res) => {
  // 画像ファイル削除
  db.get(
    "SELECT * FROM images WHERE id = (?)",
    req.params.imageId,
    (err, row) => {
      if (err) console.log({ err });
      fs.unlink(row.path, err => {
        if (err) {
          console.log(err);
          throw err;
        }
        console.log("削除しました。");
      });
      return;
    }
  );
  const sql = "DELETE FROM images WHERE id = ?";
  const params = req.params.imageId;
  db.run(sql, params, (err, result) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: `DELETE HTTP method on image/${req.params.imageId}`,
      changes: this.changes
    });
  });
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log("listen on port " + port);
});
