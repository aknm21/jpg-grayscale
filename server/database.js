const sqlite3 = require("sqlite3").verbose();

let db = new sqlite3.Database(
  "server/db/sqlite.db",
  sqlite3.OPEN_READWRITE,
  err => {
    if (err) {
      console.error(err.message);
      throw err;
    } else {
      console.log("Connected to the SQLite database.");
      db.serialize(() => {
        db.run(
          `CREATE TABLE IF NOT EXISTS Images (
            id INTEGER PRIMARY KEY autoincrement,
            title TEXT NOT NULL,
            filename TEXT NOT NULL,
            path TEXT NOT NULL
          )`,
          err => {
            if (err) {
              console.log(err);
            }
          }
        );
      });
    }
  }
);

module.exports = db;
